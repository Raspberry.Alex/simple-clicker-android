package com.example.ravenclicker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button mCrowsCounterButton;
    private Button mCatsCounterButton;
    private int mCount = 0;
    private int mCatsCounter = 0;
    TextView mInfoTextView;
    TextView mInfoTextView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCrowsCounterButton = (Button)findViewById(R.id.buttonCrowsCounter);
        mCatsCounterButton = (Button)findViewById(R.id.buttonCatsCounter);
    }


       public void onClick(View view) {

        mInfoTextView = (TextView)findViewById(R.id.buttonCrowsCounter);
           mInfoTextView1 = (TextView)findViewById(R.id.buttonCatsCounter);
        mCrowsCounterButton.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   {
                       mInfoTextView.setText("Я насчитал " + ++mCount + " ворон");
                   }
               }
           });
           mCatsCounterButton.setOnClickListener(new View.OnClickListener(){
               @Override
               public void onClick(View w){
                   {
                       mInfoTextView1.setText("Я насчитал "+ ++mCatsCounter + " котов");
                   }
               }
           });
    }


}
